"use strict";

const { parallel, watch, src, dest, series, lastRun } = require('gulp');


var browserSync = require('browser-sync'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    fs = require('fs'),
    sass = require('gulp-sass');
    sass.compiler = require('node-sass');


async function browsersync() {
    browserSync.init({
        proxy: "http://livro.cantos/", // local php address
        notify: true,
        watch: true
    });
}

async function reload(){
    browserSync.reload()  
}

async function style() {
    return src('./css/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(dest('./css'));
  };
   

async function bundler(){

    var b = browserify({
      entries: ['./js/main.js'],
      cache: {},
      packageCache: {},
      plugin: [watchify]
    });

    b.on('update', bundle);

    async function bundle() {
      b.bundle()
        .on('error', console.error)
        .pipe(fs.createWriteStream('./js/bundle.js'))
      ;
    }

    bundle();

}


watch(['*.php', './js/*', './css/*']).on('change', parallel( reload, style ));

exports.default = parallel(browsersync, bundler) ;