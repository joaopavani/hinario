           <div class="row mt-3">
                <div class="col-lg-2 d-none d-lg-block bg-light">
                    <span class="mt-2 mb-2 d-block">Filtrar pelo momento da celebração</span>
                    <div class="button-group col-12 filter-button-group btn-group-vertical">
                        <button class="todos btn btn-outline-dark" data-filter="0">Todos</button>
                        <button class="filtro btn btn-outline-dark" data-filter="21">Mantra</button>
                        <button class="filtro btn btn-outline-dark" data-filter="1">Entrada</button>
                        <button class="filtro btn btn-outline-dark" data-filter="19">Coroa do Advento</button>
                        <button class="filtro btn btn-outline-dark" data-filter="2">Ato Penitencial</button>
                        <button class="filtro btn btn-outline-dark" data-filter="15">Aspersão</button>
                        <button class="filtro btn btn-outline-dark" data-filter="3">Glória</button>
                        <button class="filtro btn btn-outline-dark" data-filter="16">Entrada da bíblia</button>
                        <button class="filtro btn btn-outline-dark" data-filter="4">Salmo</button>
                        <button class="filtro btn btn-outline-dark" data-filter="18">Sequência</button>
                        <button class="filtro btn btn-outline-dark" data-filter="5">Aclamação</button>
                        <button class="filtro btn btn-outline-dark" data-filter="6">Creio</button>
                        <button class="filtro btn btn-outline-dark" data-filter="7">Procissão das oferendas</button>
                        <button class="filtro btn btn-outline-dark" data-filter="8">Santo</button>
                        <button class="filtro btn btn-outline-dark" data-filter="9">Pai Nosso</button>
                        <button class="filtro btn btn-outline-dark" data-filter="10">Paz</button>
                        <button class="filtro btn btn-outline-dark" data-filter="11">Cordeiro de Deus</button>
                        <button class="filtro btn btn-outline-dark" data-filter="12">Comunhão</button>
                        <button class="filtro btn btn-outline-dark" data-filter="13">Pós Comunhão</button>
                        <button class="filtro btn btn-outline-dark" data-filter="14">Final</button>
                        <button class="filtro btn btn-outline-dark" data-filter="22">Marianos</button>
                        <button class="filtro btn btn-outline-dark" data-filter="17">Diversos</button>
                    </div>
                </div>
                <div class="col-lg-10">

                    

                    <div class="input-group">
                        <input id="busca" type="text" class="form-control"
                            placeholder="Procure pelo nome ou pelo número da música"
                            aria-label="Procure pelo nome ou pelo número da música">
                    </div>

                    <ul class="list-unstyled mt-3" id="lista"></ul>
                </div>
            </div>
