'use strict';

global.jQuery = require('jquery');
const {
  each,
  data
} = require('jquery');

var $ = require('jquery'),
  moment = require('moment'),
  Cookies = require('js-cookie/src/js.cookie');

require('moment/locale/pt-br');

var app = {

  init: function () {

    $.ajax({
      url: './lista_cantos.php',

    }).done(function (data) {

      app.lista(JSON.parse(data));

    });

    app.formatEstrofe();
    app.editarInput();
    app.cabecalho();
    app.busca();
    app.tamanhoFonte();
    app.cifra();
    app.sugestoes();
    app.initTabs();



    let params = new URLSearchParams(document.location.search.substring(1)),
      ativo = params.get("ativo");

    if (ativo == 0) {
      app.musicaRemovida();
    };

    $('#imprimir').on('click', function () {
      window.print();
    });

  },

  initTabs: function () {

    var triggerTabList = [].slice.call(document.querySelectorAll('#mainTab a'))
    triggerTabList.forEach(function (triggerEl) {
      var tabTrigger = new bootstrap.Tab(triggerEl)

      triggerEl.addEventListener('click', function (event) {
        event.preventDefault()
        tabTrigger.show()
      })
    })

  },

  busca: () => {

    $('#busca').on('keyup', (e) => {

      e.preventDefault();

      var pesquisa = $('#busca').val().toLowerCase();

      $.each($('#lista li'), function () {

        var nomeMusica = $(this).data('pesquisa'),

          corresponde = nomeMusica.toLowerCase().indexOf(pesquisa) >= 0;

        $(this).css('display', corresponde ? '' : 'none');

      })

    });

  },

  tamanhoFonte: function (e) {
    var elemento = $(".musica"),
      fonte = elemento.css('font-size');

    $("#aumentar").on('click', function () {
      elemento.css("fontSize", parseInt(fonte) + 1);
    })

    $("#diminuir").on('click', function () {
      elemento.css("fontSize", parseInt(fonte) - 1);
    })
  },

  cifra: function () {

    $("#mostrar").hide();

    $('#esconder').on('click', function () {

      $('.cifra').hide();

      $($('.linha.refrao')[0]).addClass('mt-3');

      $.each($('.linha'), function () {

        if ($(this).text().match(/\d/gm)) {
          $(this).addClass('mt-3')
        };
      })

      $(this).hide();

      $("#mostrar").show();

    });

    $('#mostrar').on('click', function () {

      $('.cifra').show();

      $($('.linha.refrao')[0]).removeClass('mt-3');

      $.each($('.linha'), function () {

        if ($(this).text().match(/\d/gm)) {
          $(this).removeClass('mt-3')
        };
      })

      $(this).hide();

      $("#esconder").show();

    })

  },

  lista: function (data) {

    $.each(data, function (k, v) {

      if (v.nome) {
        $('#lista').append(
          '<li class"row item data-id="' + v.numero + '" data-ativo="' + v.ativo + '" data-momento="' + v.momento + '" data-tempo="' + v.tempo + '" data-pesquisa="' + v.numero + v.nome + v.nome_popular + '">' +
          '<div class"titulo col-12" ><h3>' + ("0000" + v.numero).slice(-4) + " - " + v.nome + '</h3></div>' +
          '<small> Nome popular: ' + v.nome_popular + '</small>' +
          '<div class"row"> Tom: ' + v.tom + " &bull; " + app.momento(v.momento) + '</div>' +
          '</li>'
        );
      } else {
        $('#lista').append(
          '<li class"row item data-id="' + v.numero + '" data-ativo="' + v.ativo + '" data-momento="' + v.momento + '" data-tempo="' + v.tempo + '" data-pesquisa="' + v.numero + v.nome_popular + '">' +
          '<div class"titulo col-12" ><h3>' + ("0000" + v.numero).slice(-4) + " - " + v.nome_popular + '</h3></div>' +
          '<small> Nome popular: ' + v.nome_popular + '</small>' +
          '<div class"row"> Tom: ' + v.tom + " &bull; " + app.momento(v.momento) + '</div>' +
          '</li>'
        );
      }

    })

    app.buildMusica();
    app.filtro();

  },

  momento: function (data) {

    var codigos = {
      "1": "Entrada",
      "2": "Ato Penitencial",
      "3": "Glória",
      "4": "Salmo",
      "5": "Aclamação",
      "6": "Creio",
      "7": "Ofertório",
      "8": "Santo",
      "9": "Pai Nosso",
      "10": "Paz",
      "11": "Cordeiro",
      "12": "Comunhão",
      "13": "Pós Comunhão",
      "14": "Final",
      "15": "Aspersão",
      "16": "Procissão da Bíblia",
      "17": "Diversos",
      "18": "Seqüência",
      "19": "Coroa do Advento",
      "20": "Pregão Pascal",
      "21": "Mantra",
      "22": "Marianos"
    }


    for (var i in codigos) {

      return codigos[data];

    }
  },

  tempo: function (data) {

    var codigos = {
      "1": "Advento",
      "2": "Natal",
      "3": "Tempo Comum",
      "4": "Quaresma",
      "5": "Páscoa",
      "6": "Missões",
      "7": "Vocações",
      "8": "Bíblia",
      "9": "Maria",
      "10": "Santíssima Trindade",
      "11": "Epifania",
      "12": "Pentecostes",
      "13": "Finados",
      "14": "Cristo Rei",
      "15": "Todos os Santos",
      "16": "Ascensão do Senhor",
      "17": "Batismo do Senhor",
      "18": "Passionistas"
    }

    for (var i in codigos) {

      return codigos[data];

    }

  },

  formatEstrofe: function () {

    var estrofe = $('pre'),
        refrao = $('pre.refrao');

    refrao.first().addClass('mt-4');

    refrao.last().addClass('mb-4');

    $.each(estrofe, function () {

      if ($(this).data('estrofe') >= 1) {

        $(this).addClass('mt-4');

      }

    });

  },

  editarInput: function () {

    var input = $('.edit-line'),
      id = 0,
      info = [],
      linha = [];


    $.each(input, function () {

      id++;

      linha.push({
        "id": id,
        "texto": $(this).find('input')[0].value,
        "cifra": $(this).find('input')[1].checked,
        "letra": $(this).find('input')[2].checked,
        "refrao": $(this).find('input')[3].checked
      });

    });


    // $('#salvar').on('click', function(linha){

    //   $.ajax({
    //     type: 'POST',
    //     url: '../classes/edit.php',
    //     data: linha
    //   })

    // });
  },

  cabecalho: function () {

    var anoAtual = moment().year(),
      ref = 0,
      razao = '';

    var yearArr = String(anoAtual).split("").map((num) => {
      return Number(num)
    })

    for (var i = 0; i < yearArr.length; i++) {
      ref += yearArr[i];
    }

    // razao = ref % 3;

    // $(".data-atual").html(
    //   moment().format('dddd [,] LL') + " - Ano " + app.letraAno(razao)
    // );

    // Cookies.set('ano', app.letraAno(razao))

    $('.ano-tab').on('click', function () {
      Cookies.set('ano', $(this).data('ano'))
    })

  },

  letraAno: function (data) {

    var anos = {
      "1": "A",
      "2": "B",
      "0": "C"
    };

    for (var i in anos) {

      return anos[data];

    }

  },

  filtro: function () {

    var items = $("#lista li"),
      filtro = $(".filtro"),
      todos = $(".todos");

    filtro.on('click', function () {
      var filtroVal = $(this).data();

      $.each(items, function () {

        if ($(this).data('momento') != null && $(this).data('momento') == filtroVal.filter) {

          $(this).show();

        } else {
          $(this).hide();
        }

      })

    })

    todos.on('click', function () {
      items.show();
    })

  },

  buildMusica: function () {

    $("#lista").find('li').on('click', function () {

      window.open(window.location.href.replace('home', '') + 'musica.php?id=' + $(this).data('id') + '&ativo=' + $(this).data('ativo'), '_blank');

    });
  },

  musicaRemovida: function () {

    var motivos = $('#motivos').data('motivos');

    $.ajax({
      url: "data/inadequacoes.json",
      dataType: 'json'
    }).done(function (inadequacoes) {

      if (motivos.length > 1) {
        motivos = motivos.split(';');

        $.each(motivos, function (k, motivo) {

          $('#motivos .level-1').append(
            '<li class="mt-3"><b>' + inadequacoes[motivo].nome + '</b><br>' + inadequacoes[motivo].descricao +
            '</li>');

          $.each(inadequacoes[motivo].detalhes, function (k, detalhe) {

            $('#motivos .level-1').append(
              '<ul><li class="mt-3"><b>' + detalhe.nome + '</b><br>' + detalhe.descricao +
              '</li></ul>'
            );

          });

        });

      } else {

        $('#motivos .level-1').append(
          '<li class="mt-3"><b>' + inadequacoes[motivos].nome + '</b><br>' + inadequacoes[motivos].descricao +
          '</li>');

        $.each(inadequacoes[motivos].detalhes, function (k, detalhe) {

          $('#motivos .level-1').append(
            '<ul><li class="mt-3"><b>' + detalhe.nome + '</b><br>' + detalhe.descricao +
            '</li></ul>'
          );

        });

      }

    })

  },
  sugestoes: function () {

    $.getJSON({
        url: './data/sugestoes.json'
      })
      .done(function (data) {

        $('.link-data').on('click', function () {

          console.log($(".sugestao-" + Cookies.get('ano') + " h4"));

          $(".sugestao-" + Cookies.get('ano') + " h4, #lista-dia").text('');


          window.scrollTo(0, 0);

          global.aTempo = $(this).data('tempo');
          global.aDia = $(this).data('dia');

          $.each(data, function () {

            if (this.ano == Cookies.get('ano')) {

              $.each(this.tempo, function () {

                if (this.nome == aTempo) {

                  $(".sugestao-" + Cookies.get('ano') + " h4").text('').html('<small>do ' + this.nome + '</small>');

                  $.each(this.dias, function () {

                    app.montaListaSugestoes(this);

                  });

                };

              });

            } else if (this.ano == 'Dias Santos de Guarda' || this.ano == 'Trezena e Festa de Santo Antonio' || this.ano == 'Novena e Solenidade de Nossa Senhora Aparecida') {

              $.each(this.lista, function () {

                app.montaListaSugestoes(this);

              });

            } else if (this.ano == 'Outros') {

              $.each(this.lista, function () {

                if (this.dia == aDia) {

                  $(".sugestao-" + Cookies.get('ano') + " h3").html(this.dia);
                  $('#lista-dia').html('');

                  $.each(this.cantos, function (k, canto) {

                    $('#lista-dia').append(
                      '<li class="mb-2"><a href="/hinario/musica.php?id=' + canto.id + '" target="_blank">' + canto.id + ' - ' + canto.nome + '</a></li>'
                    );

                  });

                }

              });
            }

          })

        });

      });

  },

  montaListaSugestoes: function (lista) {

    if (lista.dia == aDia) {

      $(".sugestao-" + Cookies.get('ano') + " h3").html(lista.dia + " ");

      $.each(lista.cantos, function (k, opt) {

        $('.' + Cookies.get('ano')).html(
          "<li class='mb-2'>Entrada: <ul id='entrada'></ul></li>" +
          "<li class='mb-2'>Ato Penitencial: <ul id='ato'></ul></li>" +
          "<li class='mb-2'>Hino de Louvor: <ul id='gloria'></ul></li>" +
          "<li class='mb-2'>Aclamação ao Evangelho: <ul id='aclamacao'></ul></li>" +
          "<li class='mb-2'>Procissão das Oferendas: <ul id='oferendas'></ul></li>" +
          "<li class='mb-2'>Santo: <ul id='santo'></ul></li>" +
          "<li class='mb-2'>Cordeiro de Deus: <ul id='cordeiro'></ul></li>" +
          "<li class='mb-2'>Comunhão: <ul id='comunhao'></ul></li>" +
          "<li class='mb-2'>Final: <ul id='final'></ul></li>"
        );

        function li(dado) {

          if (dado.youtube) {

            return '<li class="mb-1 border-bottom p-1"><a href="/hinario/musica.php?id=' + dado.id + '" target="_blank" class="text-decoration-none">' + dado.id + ' - ' + dado.nome + '</a><br>' +
              '<i class="fab fa-youtube text-danger"></i> <a href="' + dado.youtube + '" target="_blank" class="text-decoration-none">' + dado.youtube + '</a>' +
              '</li>';

          } else {

            return '<li class="mb-1 border-bottom p-1"><a href="/hinario/musica.php?id=' + dado.id + '" target="_blank" class="text-decoration-none">' + dado.id + ' - ' + dado.nome + '</a></li>';
          }

        }


        $.each(opt.entrada, function () {
          $('#lista-dia #entrada').append(li(this));
        });
        $.each(opt.ato, function () {
          $('#lista-dia #ato').append(li(this));
        });
        $.each(opt.gloria, function () {
          $('#lista-dia #gloria').append(li(this));
        });
        $.each(opt.aclamacao, function () {
          $('#lista-dia #aclamacao').append(li(this));
        });
        $.each(opt.oferendas, function () {
          $('#lista-dia #oferendas').append(li(this));
        });
        $.each(opt.santo, function () {
          $('#lista-dia #santo').append(li(this));
        });
        $.each(opt.cordeiro, function () {
          $('#lista-dia #cordeiro').append(li(this));
        });
        $.each(opt.comunhao, function () {
          $('#lista-dia #comunhao').append(li(this));
        });
        $.each(opt.final, function () {
          $('#lista-dia #final').append(li(this));
        });

      });

    };

  }

}

$(function () {
  app.init();
})