
    <div class="row mt-3">
        <div class="col-6" id="lista-sugestao">
            <?php
    
        $dados = file_get_contents('./data/sugestoes.json');
        $arr = json_decode($dados, true);

        foreach($arr as $ano){ 
        
        if($ano['ano'] == 'C'){ ?>
            <h2 class="h3">Ano C</h2>

            <ul><?php

            foreach($ano['tempo'] as $tempo){ ?>

                <li class="tempo"><?php echo $tempo['nome']; ?>
                    <ul>
                        <?php foreach($tempo['dias'] as $dia){?>

                        <li><a class="link-data" data-tempo="<?php echo $tempo['nome']; ?>"
                                data-dia="<?php echo $dia['dia'];?>"><?php echo $dia['dia'];?></a></li>

                        <?php };?>

                    </ul>
                </li>

                <?php }; ?>

            </ul>

            <?php }; ?>

            <?php if($ano['ano'] == "Dias Santos de Guarda"){ ?>

            <h2 class="h3">Solenidades e festas <br><small>(Dias Santos de Guarda)</small></h2>

            <ul><?php foreach( $ano['lista'] as $lista ){ ?>

                <li><a class="link-data" data-dia="<?php echo $lista['dia'];?>"> <?php echo $lista['dia']; ?></a></li>

                <?php }; ?>
            </ul>

            <?php }; ?>

            <?php if($ano['ano'] == "Trezena e Festa de Santo Antonio"){ ?>

            <h2 class="h3">Trezena e Festa de Santo Antonio</h2>

            <ul><?php foreach( $ano['lista'] as $lista ){ ?>

                <li><a class="link-data" data-dia="<?php echo $lista['dia'];?>"> <?php echo $lista['dia']; ?></a></li>

                <?php }; ?>
            </ul>

            <?php }; ?>
<!--  -->
            <?php if($ano['ano'] == "Novena e Solenidade de Nossa Senhora Aparecida"){ ?>

            <h2 class="h3">Novena e Solenidade de Nossa Senhora Aparecida</h2>

            <ul><?php foreach( $ano['lista'] as $lista ){ ?>

                <li><a class="link-data" data-dia="<?php echo $lista['dia'];?>"> <?php echo $lista['dia']; ?></a></li>

                <?php }; ?>
            </ul>

            <?php }; ?>
<!--  -->

            <?php if($ano['ano'] == "Outros"){ ?>

            <h2 class="h3">Outros</h2>

            <ul><?php foreach( $ano['lista'] as $lista ){ ?>

                <li><a class="link-data" data-dia="<?php echo $lista['dia'];?>"> <?php echo $lista['dia']; ?></a></li>

                <?php }; ?>
            </ul>

            <?php }; ?>

            <?php }; ?>

        </div>

        <div class="col-6 sugestao-C" id="">

            <h3></h3>
            <h4></h4>
            
            <ul id='lista-dia' class="C"></ul>
        </div>
    </div>

