<?php require_once('includes/header.php'); ?>
<?php 

require_once('classes/usuarios.php'); 

$user = new Usuario;

?>

<div class="container">

    <h1 class="text-center mt-3 mb-3">Hinário - Catedral Santo Antônio</h1>
    <hr>

    <form method="post" class="col-3 mx-auto">
        <div class="mb-3">
            <label for="email" class="form-label">E-mail</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="nome@email.com">
        </div>
        <div class="mb-3">
            <label for="senha" class="form-label">Senha</label>
            <input type="password" class="form-control" name="senha" id="senha" placeholder="00000000" maxlength="8">
        </div>
        <button class="btn btn-dark">Entrar</button>
    </form>
</div>

<?php

if(isset($_POST['email'])){

    $email = addslashes($_POST['email']);
    $senha = addslashes($_POST['senha']);

    if(!empty($email) && !empty($senha)){

        $x = $user->conectar('hinario.mysql.dbaas.com.br','hinario', $_ENV['MASTER_USER'], $_ENV['MASTER_PASS']);
        
        if($user->msgErro == ""){
            
            if($user->logar($email,$senha)){
                
                header('Location: home');

            }else{ ?>
            
            <div class="container">
               <div class="col-3 mx-auto text-danger mt-2"><i>E-mail e/ou senha incorretos</i></div> 
            </div>
<?php
            }
        }else{

            echo $user->msgErro;
        }

    }else{ ?>
            <div class="container">
               <div class="col-3 mx-auto text-danger mt-2"><i>Preencha todos os campos</i></div> 
            </div>
<?php
    }
}


?>

<?php require_once('includes/footer.php'); ?>