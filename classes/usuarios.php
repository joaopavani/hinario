<?php

Class Usuario{

    private $pdo;
    public $msgErro = "";

    public function conectar($host, $nome, $usuario, $senha){

        try {

            global $pdo;
            $pdo = new PDO('mysql:host='.$host.';dbname='.$nome, $_ENV['MASTER_USER'], $_ENV['MASTER_PASS']);

        } catch (PDOException $e) {

            $msgErro = $e->getMessage();

        }
        
    }

    public function logar($email, $senha){

        global $pdo;  
        
        $sql = $pdo->prepare("SELECT id FROM usuarios WHERE email = :e AND senha = :s");
        $sql->bindValue(":e", $email);
        $sql->bindValue(":s", md5($senha));
        $sql->execute();

        if($sql->rowCount() > 0){

            $dado = $sql->fetch();
            session_start();
            $_SESSION['id'] = $dado['id'];
            return true;

        }else{

            return false;

        }


    }

}






?>