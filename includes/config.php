<?php
// .ENV
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Connect MySQL DB

try {

    $db = new PDO('mysql:host=hinario.mysql.dbaas.com.br;dbname=hinario;charset=utf8', $_ENV['MASTER_USER'], $_ENV['MASTER_PASS']);

} catch (PDOException $e) {
    $msgErro = $e->getMessage();

    echo $msgErro;
}

//connect Mongo DB
//*Não há erro em MongoDB\Client

$client = new MongoDB\Client(
    'mongodb+srv://'.$_ENV['MONGO_USER'].':'.$_ENV['MONGO_PASS'].'@cluster0.v9tfb.mongodb.net/liturgia?retryWrites=true&w=majority');
    
$mongodb = $client->hinario;

?>

