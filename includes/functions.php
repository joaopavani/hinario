<?php

function titulo($db){
    $query = 'SELECT *
    FROM cantos
    LEFT JOIN momentos_canto 
    ON cantos.id = momentos_canto.numero
    LEFT JOIN tempos_canto
    ON cantos.id = tempos_canto.id
    WHERE cantos.id = '.$_GET['id'];

    foreach( $db->query($query, PDO::FETCH_ASSOC) as $titulo){
        
        return [
            'all' => $titulo,
            'id' => $titulo['id'],
            'nome' => $titulo['nome'],
            'nome_popular' => $titulo['nome_popular'],
            'tom' => $titulo['tom'],
            'numero' => $titulo['numero']
        ];

    } 
}

function cd($db)
{

    $query = 'SELECT *
    FROM cd_canto
    LEFT JOIN cd
    ON cd_canto.canto_id = cd.id_cd
    WHERE cd_canto.canto_id = '.$_GET['id'];

foreach( $db->query($query, PDO::FETCH_ASSOC) as $cd){

     return [
        'nome' => $cd['nome_cd'],
        'interprete' => $cd['interprete'],
        'editora' => $cd['editora']
    ];

}


}

function musica($db){

    $query = 'SELECT *
    FROM cantos_cifrados
    WHERE cantos_cifrados.canto_id = '.$_GET['id'];

    foreach( $db->query($query, PDO::FETCH_ASSOC) as $musica){ 

        if($musica['monospace'] == 1){

            if($musica['refrao'] == 1){
                echo '<pre class="cifra refrao mono" data-estrofe="'.$musica['estrofe'].'">'.$musica['cifra'].'</pre>';
                echo '<pre class="linha fw-bold refrao mono" data-editable data-name="letra">'.$musica['linha'].'</pre>';
            }else{
                echo '<pre class="cifra mono" data-estrofe="'.$musica['estrofe'].'">'.$musica['cifra'].'</pre>';
                echo '<pre class="linha mono" data-editable data-name="letra">'.$musica['linha'].'</pre>';
                // echo '<pre class="linha" data-editable data-name="letra">'.corrigeNomes(ucfirst(mb_strtolower($musica['linha'], 'UTF-8'))).'</pre>'; // Caixa baixa
            }

        }else{
            if($musica['refrao'] == 1){
                echo '<pre class="cifra refrao " data-estrofe="'.$musica['estrofe'].'">'.$musica['cifra'].'</pre>';
                echo '<pre class="linha fw-bold refrao " data-editable data-name="letra">'.$musica['linha'].'</pre>';
            }else{
                echo '<pre class="cifra " data-estrofe="'.$musica['estrofe'].'">'.$musica['cifra'].'</pre>';
                echo '<pre class="linha " data-editable data-name="letra">'.$musica['linha'].'</pre>';
                // echo '<pre class="linha" data-editable data-name="letra">'.corrigeNomes(ucfirst(mb_strtolower($musica['linha'], 'UTF-8'))).'</pre>'; // Caixa baixa
            }
        }
    }
    
}

function editaMusica($db){

    $query = 'SELECT *
    FROM cantos_cifrados
    WHERE cantos_cifrados.canto_id = '.$_GET['id'];

    $num = 0;

    foreach( $db->query($query, PDO::FETCH_ASSOC) as $musica){

        $num++;

        if($musica['refrao'] != 1){

            echo    '<div class="edit-line" data-id="'.$num.'">'.
                        '<div class="input-group"><input type="text" class="w-100 text-danger fw-bold m-2" value="'.$musica['cifra'].'"/></div>'.
                        '<div class="row row-cols-auto align-items-center justify-content-end">'.
                            
                            '<div class="form-check">'.
                                '<input class="form-check-input radio-cifra" type="radio" name="cifra-'.$num.'" value="cifra" checked>'.
                                '<label class="form-check-label">Cifra</label>'.
                            '</div>'.
                            '<div class="form-check">'.
                                '<input class="form-check-input" type="radio" name="cifra-'.$num.'" value="letra">'.
                                '<label class="form-check-label" for="flexRadioDefault2">Letra</label>'.
                            '</div>'.
                            ' | &nbsp;'. 
                            '<div class="form-check form-switch">'.
                                '<input class="form-check-input switch" type="checkbox">'.
                                '<label class="form-check-label" for="flexSwitchCheckDefault">Refrão</label>'.
                            '</div>'.
                            ' |'. 
                            '<div>'.
                                '<a class="btn btn-danger m-2" role="button"><i class="far fa-trash-alt"></i> REMOVER LINHA</a>'.
                            '</div>'.                   
                        '</div>'.
                    '</div>'.
                    '<div class="row row-cols-auto justify-content-end"><a class="text-decoration-none m-2" role="button"><i class="far fa-plus-square"></i> ADICIONAR LINHA</a></div>';

            echo    '<div class="edit-line" data-id="'.$num.'">'.
                    '<div class="input-group"><input type="text" class="w-100 m-2" value="'.$musica['linha'].'"/></div>'.
                    '<div class="row row-cols-auto align-items-center justify-content-end">'.
                        
                        '<div class="form-check">'.
                            '<input class="form-check-input radio-cifra" type="radio" name="letra-'.$num.'" value="cifra" >'.
                            '<label class="form-check-label">Cifra</label>'.
                        '</div>'.
                        '<div class="form-check">'.
                            '<input class="form-check-input" type="radio" name="letra-'.$num.'" value="letra" checked>'.
                            '<label class="form-check-label" for="flexRadioDefault2">Letra</label>'.
                        '</div>'.
                        ' | &nbsp;'. 
                        '<div class="form-check form-switch">'.
                            '<input class="form-check-input switch" type="checkbox">'.
                            '<label class="form-check-label" for="flexSwitchCheckDefault">Refrão</label>'.
                        '</div>'.
                        ' |'. 
                        '<div>'.
                            '<a class="btn btn-danger m-2" role="button"><i class="far fa-trash-alt"></i> REMOVER LINHA</a>'.
                        '</div>'.                   
                    '</div>'.
                '</div>'.
                '<div class="row row-cols-auto justify-content-end"><a class="text-decoration-none m-2" role="button"><i class="far fa-plus-square"></i> ADICIONAR LINHA</a></div>';
            }else{
                echo    '<div class="edit-line" data-id="'.$num.'">'.
                        '<div class="input-group"><input type="text" class="w-100 text-danger fw-bold m-2" value="'.$musica['cifra'].'"/></div>'.
                        '<div class="row row-cols-auto align-items-center justify-content-end">'.
                            
                            '<div class="form-check">'.
                                '<input class="form-check-input radio-cifra" type="radio" name="cifra-'.$num.'" value="cifra" checked>'.
                                '<label class="form-check-label">Cifra</label>'.
                            '</div>'.
                            '<div class="form-check">'.
                                '<input class="form-check-input" type="radio" name="cifra-'.$num.'" value="letra" >'.
                                '<label class="form-check-label" for="flexRadioDefault2">Letra</label>'.
                            '</div>'.
                            ' | &nbsp;'. 
                            '<div class="form-check form-switch">'.
                                '<input class="form-check-input switch" type="checkbox" checked>'.
                                '<label class="form-check-label" for="flexSwitchCheckDefault">Refrão</label>'.
                            '</div>'.
                            ' |'. 
                            '<div>'.
                                '<a class="btn btn-danger m-2" role="button"><i class="far fa-trash-alt"></i> REMOVER LINHA</a>'.
                            '</div>'.                   
                        '</div>'.
                    '</div>'.
                    '<div class="row row-cols-auto justify-content-end"><a class="text-decoration-none m-2" role="button"><i class="far fa-plus-square"></i> ADICIONAR LINHA</a></div>';

            echo    '<div class="edit-line" data-id="'.$num.'">'.
                    '<div class="input-group"><input type="text" class="w-100 fw-bold m-2" value="'.$musica['linha'].'"/></div>'.
                    '<div class="row row-cols-auto align-items-center justify-content-end">'.
                        
                        '<div class="form-check">'.
                            '<input class="form-check-input radio-cifra" type="radio" name="letra-'.$num.'" value="cifra" >'.
                            '<label class="form-check-label">Cifra</label>'.
                        '</div>'.
                        '<div class="form-check">'.
                            '<input class="form-check-input" type="radio" name="letra-'.$num.'" value="letra" checked>'.
                            '<label class="form-check-label" for="flexRadioDefault2">Letra</label>'.
                        '</div>'.
                        ' | &nbsp;'. 
                        '<div class="form-check form-switch">'.
                            '<input class="form-check-input switch" type="checkbox" checked>'.
                            '<label class="form-check-label" for="flexSwitchCheckDefault">Refrão</label>'.
                        '</div>'.
                        ' |'. 
                        '<div>'.
                            '<a class="btn btn-danger m-2" role="button"><i class="far fa-trash-alt"></i> REMOVER LINHA</a>'.
                        '</div>'.                   
                    '</div>'.
                '</div>'.
                '<div class="row row-cols-auto justify-content-end"><a class="text-decoration-none m-2" role="button"><i class="far fa-plus-square"></i> ADICIONAR LINHA</a></div>';
            
            };
    }
    
}

function corrigeNomes($frase){
    
    $errado = [
        1 => "senhor",
        2 => "deus",
        3 => "jesus",
        4 => "maria",
        5 => "josé",
        6 => "antônio",
        7 => "antonio",
        8 => "santo",
        9 => "santa",
        10 => "teresinha",
        11 => "pai",
        12 => "cristo",
        13 => "espírito"
    ];

    $correto = [
        1 => "Senhor",
        2 => "Deus",
        3 => "Jesus",
        4 => "Maria",
        5 => "José",
        6 => "Antônio",
        7 => "Antônio",
        8 => "Santo",
        9 => "Santa",
        10 => "Teresinha",
        11 => "Pai",
        12 => "Cristo",
        13 => "Espírito"
    ];

    echo str_replace($errado, $correto, $frase);

}



?>