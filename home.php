<?php 

    session_start();
    if(!isset($_SESSION['id'])){

        header('Location: index.php');
        exit;
    }

?>

<?php require_once('includes/header.php'); ?>

<div class="container">



    <div class="row mt-3 mb-3">
        <h1 class="text-center col-md-11 col-sm-8">Hinário - Catedral Santo Antônio</h1>
        <a href="sair.php" class="btn btn-light col-md-1 col-sm-2 align-self-center">Sair</a>
    </div>
    <div class="data-atual mb-3"></div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active ano-tab" data-ano='B' id="anob-tab" data-bs-toggle="tab" data-bs-target="#anob" type="button"
                role="tab" aria-controls="anob" aria-selected="true">Ano B</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link ano-tab" data-ano='C' id="anoc-tab" data-bs-toggle="tab" data-bs-target="#anoc" type="button"
                role="tab" aria-controls="anoc" aria-selected="true">Ano C</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button"
                role="tab" aria-controls="profile" aria-selected="false">Lista</button>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="anob" role="tabpanel" aria-labelledby="anob-tab"><?php include_once('sugestoes_B.php'); ?></div>
        <div class="tab-pane fade" id="anoc" role="tabpanel" aria-labelledby="anoc-tab"><?php include_once('sugestoes_C.php'); ?></div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><?php include_once('lista.php'); ?></div>
    </div>

 

</div>


<?php require_once('includes/footer.php'); ?>