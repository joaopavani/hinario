<?php
    session_start();
    if(!isset($_SESSION['id'])){

        header('Location: index.php');
        exit;
    }
    
    require_once('includes/header.php');
    require_once('includes/functions.php');
?>

<div class="container">

    <div class="row mt-2 mb-3 align-items-center">
        <!-- <a onclick="history.back()" class="btn btn-primary btn-sm col no-print mb-2">voltar</a> -->
        <small class="text-center d-block col-11 no-print">Hinário - Catedral Santo Antônio</small>
        <hr>
    </div>

    <div class="row no-print">
        <div class="col-lg-8 col-12">
            <h1 class="h2">

                <?php if(titulo($db)['nome']){

            echo str_pad(titulo($db)['numero'], 4, 0, STR_PAD_LEFT).' - '.titulo($db)['nome'];

            }else{

                echo str_pad(titulo($db)['numero'], 4, 0, STR_PAD_LEFT).' - '.titulo($db)['nome_popular'];

            }; ?>
            </h1>
            <p><b>Álbum:</b> <?php echo cd($db)['nome']; ?> &bull; <b>Intérprete:</b> <?php echo cd($db)['interprete']; ?> &bull; <b>Editora:</b> <?php echo cd($db)['editora']; ?></p>
            <h6>Tom: <?php echo titulo($db)['tom']; ?></h6>
        </div>
        <div class="col-lg-4 d-flex justify-content-lg-end justify-content-center">
            <!-- <button class="btn btn-light btn-sm m-2" id="aumentar"><b>A+</b><br>Aumentar letra</button>
            <button class="btn btn-light btn-sm m-2" id="diminuir"><b>A-</b><br>Diminuir letra</button> -->
            <button class="btn btn-light btn-sm m-2" id="esconder"><i class="fas fa-eye-slash"></i><br>Esconder
                cifra</button>
            <button class="btn btn-light btn-sm m-2" id="mostrar"><i class="fas fa-eye"></i><br>Mostrar cifra</button>
            <button type="button" class="btn btn-light btn-sm m-2 d-none d-md-block" id="imprimir"><i
                    class="fas fa-print"></i><br>Imprimir</button>
        </div>
    </div>

    <!-- Impresão -->
    <div class="row print d-none">
        <div class="col">
            <h1 class="h4">
                <?php if(titulo($db)['nome']){

            echo str_pad(titulo($db)['id'], 4, 0, STR_PAD_LEFT).' - '.titulo($db)['nome'];

            }else{

                echo str_pad(titulo($db)['id'], 4, 0, STR_PAD_LEFT).' - '.titulo($db)['nome_popular'];

            }; ?>
            </h1>
            <h6>Tom: <?php echo titulo($db)['tom']; ?></h6>
        </div>
    </div>
    <!-- Fim Impresão -->
    
    <?php if(titulo($db)['all']['ativo'] != 0){ ?>

    <div class="mt-5 musica print" data-editable data-name="main-content"><?php echo musica($db); ?></div>

    <?php }else{ ?>

    <div class="alert alert-warning">Este canto não é adequado à liturgia da missa e foi removido devido a um ou mais
        dos motivos listados abaixo.</div>

    <div id="motivos" data-motivos="<?php echo titulo($db)['all']['motivo_desativacao']; ?>">
        <ol class="level-1"></ol>
    </div>

    <?php }; ?>
</div>


<?php require_once('includes/footer.php'); ?>