<?php require './vendor/autoload.php'; ?>

<?php include('./includes/config.php'); ?>


        <?php 
     
        $sqlCantos = 'SELECT *
        FROM cantos

        LEFT JOIN momentos_canto
        ON cantos.id = momentos_canto.numero
 
        LEFT JOIN tempos_canto
        ON cantos.id = tempos_canto.id

        GROUP BY cantos.id';
        
        $arrCantos = [];
        
        foreach ($db->query($sqlCantos, PDO::FETCH_ASSOC) as $item) {
            
            array_push($arrCantos, $item);
            
        }

        echo json_encode($arrCantos);
        
?>


