<?php
    require_once('includes/header.php');
    require_once('includes/functions.php');
?>

<style>
@import url('https://fonts.googleapis.com/css2?family=Cousine:wght@400;700&family=Roboto+Mono:wght@400;700&display=swap');
</style>

<div class="container" style="font-family: 'Cousine','Roboto Mono', 'Courier New', 'Courier', monospace;">

    <h1 class="text-center mt-5 mb-5">Hinário - Catedral Santo Antônio</h1>

    <?php



// foreach($mongodb->cantos->find() as $item){

//     echo $item['titulo'];
// };

    ?>

    <form  class="mb-5" action="" id="editar" >

    <h2>
        <?php if(titulo($db)['nome']){

            echo '<input type="text" value="'.str_pad(titulo($db)['id'], 4, 0, STR_PAD_LEFT).'" name="numero" readonly/> - <input type="text" class="w-100" value="'.titulo($db)['nome'].'" name="titulo" />';

            }else{

                echo '<input type="text" value="'.str_pad(titulo($db)['id'], 4, 0, STR_PAD_LEFT).'" name="numero" readonly/> - <input type="text" class="w-100" value="'.titulo($db)['nome_popular'].'" name="titulo" />';

            }; ?>
    </h2>
    <p><b>Álbum:</b> <?php echo '<input type="text" value="'.cd($db)['nome'].'" name="album"/>'; ?> &bull; <b>Intérprete:</b> <?php echo '<input type="text" value="'.cd($db)['interprete'].'" name="interprete" />'; ?> &bull; <b>Editora:</b> <?php echo '<input type="text" value="'.cd($db)['editora'].'" name="editora" />'; ?></p>
    <h6>Tom: <?php echo '<input type="text" value="'.titulo($db)['tom'].'" name="tom" />'; ?></h6>

    <div class="mt-5" data-editable data-name="main-content"><?php echo editaMusica($db); ?></div>


    <button class="btn btn-primary" type="submit" id="salvar">Salvar</button>     
</form>

</div>


<?php require_once('includes/footer.php'); ?>
